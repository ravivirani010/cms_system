#!/bin/bash

# Create the public directory if it doesn't exist
mkdir -p public

# Copy all contents from _site to public
cp -r _site/* public/
