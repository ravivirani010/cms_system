---
title: "Introduction to Advanced Statistical Methods and Optimization"
---

This is the content for Advanced Statistical Methods and Optimization in the 1st semester.

## Lectures

-   [Lecture 1 - Introduction to probability](lecture1.html)
-   [Lecture 2 - Types of data](lecture2.html)
